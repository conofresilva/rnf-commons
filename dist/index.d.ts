import { PaisDto } from "./rnf-dtos/pais-dto";
import { ModulosDto } from "./rnf-dtos/modulos-dto";
import { RnfCommonService } from "./rnf-common-service/rnf-common.service";
export { PaisDto, ModulosDto, RnfCommonService };
export declare const RNF_CONSTANTS: {
    API_ROUTES: {
        TABELA: string;
        CADASTRO: string;
        OPERACAO: string;
        DATABASE: string;
    };
    API_DATABASE: {
        TABELA: {
            PAIS: string;
            ESTADO: string;
            MUNICIPIO: string;
            BAIRRO: string;
            CEP: string;
            ACESSO: string;
            MODULO_DE_ACESSO: string;
            CNAE: string;
            CNAE_SECUNDARIO: string;
            NATUREZA_JURIDICA: string;
            MOTIVO_SITUACAO_CADASTRAL_CNPJ: string;
            CNPJ: string;
            CNPJ_QUADRO_DE_SOCIO: string;
            PRODUTO: string;
            TABELA_DIVERSOS: string;
            TABELA_DIVERSOS_REGISTRO: string;
        };
        CADASTRO: {
            CONTATO: string;
            USUARIO: string;
            GRUPO_DE_USUARIO: string;
            USUARIO_GRUPO: string;
            DOMINIO_VIRTUAL: string;
        };
        OPERACAO: {
            CONTROLE_DE_ACESSO: string;
            AQUISICAO_DE_DOMINIO: string;
        };
    };
    API_RESOURCES: {
        BASE: {
            BOILERPLATE: string;
        };
        TABELA: {
            PAIS: string;
            ESTADO: string;
            MUNICIPIO: string;
            BAIRRO: string;
            CEP: string;
            ACESSO: string;
            MODULO_DE_ACESSO: string;
            CNAE: string;
            CNAE_SECUNDARIO: string;
            NATUREZA_JURIDICA: string;
            MOTIVO_SITUACAO_CADASTRAL_CNPJ: string;
            CNPJ: string;
            CNPJ_QUADRO_DE_SOCIO: string;
            PRODUTO: string;
            TABELA_DIVERSOS: string;
            TABELA_DIVERSOS_REGISTRO: string;
        };
        CADASTRO: {
            CONTATO: string;
            USUARIO: string;
            GRUPO_DE_USUARIO: string;
            USUARIO_GRUPO: string;
            DOMINIO_VIRTUAL: string;
        };
        OPERACAO: {
            CONTROLE_DE_ACESSO: string;
            AQUISICAO_DE_DOMINIO: string;
            SSE: string;
            CONTROLE_DE_EVENTO: string;
        };
    };
};
export declare const OPCAO_DE_MENU: {
    TABELA: {
        PAIS: string;
        ESTADO: string;
        MUNICIPIO: string;
        BAIRRO: string;
        CEP: string;
        ACESSO: string;
        MODULO_DE_ACESSO: string;
        CNAE: string;
        CNAE_SECUNDARIO: string;
        NATUREZA_JURIDICA: string;
        MOTIVO_SITUACAO_CADASTRAL_CNPJ: string;
        CNPJ: string;
        CNPJ_QUADRO_DE_SOCIO: string;
        PRODUTO: string;
        TABELA_DIVERSOS: string;
        TABELA_DIVERSOS_REGISTRO: string;
    };
    CADASTRO: {
        CONTATO: string;
        USUARIO: string;
        GRUPO_DE_USUARIO: string;
        DOMINIO_VIRTUAL: string;
    };
    OPERACAO: {
        AQUISICAO_DE_DOMINIO: string;
        CONTROLE_DE_ACESSO: string;
    };
    CUSTOM_QUERY: {
        USUARIO_COM_CONTATO: string;
        CONTROLE_DE_ACESSO_MODULOS: string;
        TABELA_DIVERSOS_POR_NOME: string;
    };
};
export declare const MODULOS_RNF: {
    TABELA: {
        PAIS: string;
        ESTADO: string;
        MUNICIPIO: string;
        BAIRRO: string;
        CEP: string;
        ACESSO: string;
        MODULO_DE_ACESSO: string;
        CNAE: string;
        CNAE_SECUNDARIO: string;
        NATUREZA_JURIDICA: string;
        MOTIVO_SITUACAO_CADASTRAL_CNPJ: string;
        CNPJ: string;
        CNPJ_QUADRO_DE_SOCIO: string;
        PRODUTO: string;
        TABELA_DIVERSOS: string;
        TABELA_DIVERSOS_REGISTRO: string;
    };
    CADASTRO: {
        CONTATO: string;
        USUARIO: string;
        GRUPO_DE_USUARIO: string;
        DOMINIO_VIRTUAL: string;
    };
    OPERACAO: {
        AQUISICAO_DE_DOMINIO: string;
        CONTROLE_DE_ACESSO: string;
        SSE: string;
        CONTROLE_DE_EVENTO: string;
    };
};
