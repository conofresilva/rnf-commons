"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RnfCommonService = void 0;
const common_1 = require("@nestjs/common");
let RnfCommonService = class RnfCommonService {
    dominioVirtualByRequest(request) {
        return `${request.headers['dominio-virtual'] || ''}`;
    }
    databaseHostByRequest(request) {
        return `${request.headers['database-host'] || ''}`;
    }
    databaseRouteByHost(databaseHost, request) {
        let result = `${databaseHost}${request.url}`;
        if (request.url.startsWith('/api/tabela/'))
            result = result.replace('/tabela/', '/database/');
        else if (request.url.startsWith('/api/cadastro/'))
            result = result.replace('/cadastro/', '/database/');
        else if (request.url.startsWith('/api/operacao/'))
            result = result.replace('/operacao/', '/database/');
        return result;
    }
};
RnfCommonService = __decorate([
    (0, common_1.Injectable)()
], RnfCommonService);
exports.RnfCommonService = RnfCommonService;
