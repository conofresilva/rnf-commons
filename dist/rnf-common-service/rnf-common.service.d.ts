import { Request } from 'express';
export declare class RnfCommonService {
    dominioVirtualByRequest(request: Request): string;
    databaseHostByRequest(request: Request): string;
    databaseRouteByHost(databaseHost: string, request: Request): string;
}
