"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RnfLoggerService = void 0;
const nest_winston_1 = require("nest-winston");
const winston_1 = require("winston");
var options = {
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};
exports.RnfLoggerService = nest_winston_1.WinstonModule.createLogger({
    exitOnError: false,
    format: winston_1.format.combine(winston_1.format.colorize(), winston_1.format.timestamp(), winston_1.format.printf(msg => { return `${msg.timestamp} [${msg.level}] - ${msg.message}`; })),
    transports: [
        //new transports.File(options.file),
        new winston_1.transports.Console(options.console)
    ],
});
