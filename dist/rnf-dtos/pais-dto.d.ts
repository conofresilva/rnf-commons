export declare class PaisDto {
    pais: string;
    iso2: number;
    iso3: string;
    ddi: string;
}
