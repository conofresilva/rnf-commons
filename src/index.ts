import { PaisDto } from "./rnf-dtos/pais-dto";
import { ModulosDto } from "./rnf-dtos/modulos-dto";
import { RnfCommonService } from "./rnf-common-service/rnf-common.service";

export {
    PaisDto,
    ModulosDto,
    RnfCommonService
};

const ROOT_API_TABELA   = 'api/tabela';
const ROOT_API_CADASTRO = 'api/cadastro';
const ROOT_API_OPERACAO = 'api/operacao';
const ROOT_API_DATABASE = 'api/database';

export const RNF_CONSTANTS = {

    API_ROUTES:{
        TABELA:ROOT_API_TABELA,
        CADASTRO:ROOT_API_CADASTRO,
        OPERACAO:ROOT_API_OPERACAO,
        DATABASE:ROOT_API_DATABASE
    },
    // ESTAS CONTANTES SERÃO USADAS APENAS NO MÓDULO DE DADOS
    API_DATABASE:{
        TABELA:{
            PAIS: `${ROOT_API_DATABASE}/pais`,
            ESTADO: `${ROOT_API_DATABASE}/estado`,
            MUNICIPIO: `${ROOT_API_DATABASE}/municipio`,
            BAIRRO: `${ROOT_API_DATABASE}/bairro`,            
            CEP: `${ROOT_API_DATABASE}/cep`,    
            ACESSO: `${ROOT_API_DATABASE}/acesso`,    
            MODULO_DE_ACESSO: `${ROOT_API_DATABASE}/modulo-de-acesso`,    
            CNAE: `${ROOT_API_DATABASE}/cnae`,    
            CNAE_SECUNDARIO: `${ROOT_API_DATABASE}/cnae-secundario`,    
            NATUREZA_JURIDICA: `${ROOT_API_DATABASE}/natureza-juridica`,    
            MOTIVO_SITUACAO_CADASTRAL_CNPJ: `${ROOT_API_DATABASE}/motivo-situacao-cadastral-cnpj`,    
            CNPJ: `${ROOT_API_DATABASE}/cnpj`,
            CNPJ_QUADRO_DE_SOCIO: `${ROOT_API_DATABASE}/cnpj-quadro-de-socio`,
            PRODUTO: `${ROOT_API_DATABASE}/produto`,
            TABELA_DIVERSOS: `${ROOT_API_DATABASE}/tabela-diversos`,
            TABELA_DIVERSOS_REGISTRO: `${ROOT_API_DATABASE}/tabela-diversos-registro`
        },
        CADASTRO:{
            CONTATO: `${ROOT_API_DATABASE}/contato`,            
            USUARIO: `${ROOT_API_DATABASE}/usuario`,
            GRUPO_DE_USUARIO: `${ROOT_API_DATABASE}/grupo-de-usuario`,            
            USUARIO_GRUPO: `${ROOT_API_DATABASE}/usuario-grupo`,
            DOMINIO_VIRTUAL: `${ROOT_API_DATABASE}/dominio-virtual`                    
        },
        OPERACAO:{
            CONTROLE_DE_ACESSO: `${ROOT_API_DATABASE}/controle-de-acesso`,
            AQUISICAO_DE_DOMINIO: `${ROOT_API_DATABASE}/aquisicao-de-dominio`,
            //SSE:`${ROOT_API_DATABASE}/rnf-operacao-sse` ---> não tem consulta a DB só o serviço de
            //CONTROLE_DE_EVENTO:`${ROOT_API_DATABASE}/rnf-operacao-controle-evento` ---> não tem consulta a DB só o serviço de
        }
    },
    // ESTAS CONSTANTES SERÃO USADOS NOS CONTROLLERS DOS MÓDULOS
    API_RESOURCES:{
        BASE:{
            BOILERPLATE: `api/base/boilerplate`, 
        },
        TABELA:{
            PAIS: `${ROOT_API_TABELA}/pais`,
            ESTADO: `${ROOT_API_TABELA}/estado`,
            MUNICIPIO: `${ROOT_API_TABELA}/municipio`,            
            BAIRRO: `${ROOT_API_TABELA}/bairro`,
            CEP: `${ROOT_API_TABELA}/cep`,                
            ACESSO: `${ROOT_API_TABELA}/acesso`,    
            MODULO_DE_ACESSO: `${ROOT_API_TABELA}/modulo-de-acesso`,
            CNAE: `${ROOT_API_TABELA}/cnae`,    
            CNAE_SECUNDARIO: `${ROOT_API_TABELA}/cnae-secundario`,    
            NATUREZA_JURIDICA: `${ROOT_API_TABELA}/natureza-juridica`,    
            MOTIVO_SITUACAO_CADASTRAL_CNPJ: `${ROOT_API_TABELA}/motivo-situacao-cadastral-cnpj`,                
            CNPJ: `${ROOT_API_TABELA}/cnpj`,
            CNPJ_QUADRO_DE_SOCIO: `${ROOT_API_TABELA}/cnpj-quadro-de-socio`,
            PRODUTO: `${ROOT_API_TABELA}/produto`,
            TABELA_DIVERSOS: `${ROOT_API_TABELA}/tabela-diversos`,
            TABELA_DIVERSOS_REGISTRO: `${ROOT_API_TABELA}/tabela-diversos-registro`
        },
        CADASTRO:{
            CONTATO: `${ROOT_API_CADASTRO}/contato`,            
            USUARIO: `${ROOT_API_CADASTRO}/usuario`,
            GRUPO_DE_USUARIO: `${ROOT_API_CADASTRO}/grupo-de-usuario`,            
            USUARIO_GRUPO: `${ROOT_API_CADASTRO}/usuario-grupo`,
            DOMINIO_VIRTUAL: `${ROOT_API_CADASTRO}/dominio-virtual`
        },
        OPERACAO:{
            CONTROLE_DE_ACESSO: `${ROOT_API_OPERACAO}/controle-de-acesso`,
            AQUISICAO_DE_DOMINIO: `${ROOT_API_OPERACAO}/aquisicao-de-dominio`,
            SSE:`${ROOT_API_OPERACAO}/sse`,
            CONTROLE_DE_EVENTO:`${ROOT_API_OPERACAO}/controle-de-evento`
        }
    }
}

export const OPCAO_DE_MENU = {
    TABELA: { 
        PAIS:"menu-tabela-pais",
        ESTADO: "menu-tabela-estado",
        MUNICIPIO: "menu-tabela-municipio",
        BAIRRO: "menu-tabela-bairro",
        CEP: "menu-tabela-cep",        
        ACESSO: "menu-tabela-acesso",        
        MODULO_DE_ACESSO: "menu-tabela-modulo-de-acesso",   
        CNAE: "menu-tabela-cnae",    
        CNAE_SECUNDARIO: "menu-tabela-cnae-secundario",    
        NATUREZA_JURIDICA: "menu-tabela-natureza-juridica",    
        MOTIVO_SITUACAO_CADASTRAL_CNPJ: "menu-tabela-motivo-situacao-cadastral-cnpj",            
        CNPJ: "menu-tabela-cnpj",
        CNPJ_QUADRO_DE_SOCIO: "menu-tabela-cnpj-quadro-de-socio",
        PRODUTO: "menu-tabela-produto",
        TABELA_DIVERSOS: "menu-tabela-tabela-diversos",
        TABELA_DIVERSOS_REGISTRO: "menu-tabela-tabela-diversos-registro"
    },
    CADASTRO: { 
        CONTATO:"menu-cadastro-contato",
        USUARIO:"menu-cadastro-usuario",
        GRUPO_DE_USUARIO:"menu-cadastro-grupo-de-usuario",
        DOMINIO_VIRTUAL:"menu-cadastro-dominio-virtual"
    },
    OPERACAO: {         
        AQUISICAO_DE_DOMINIO:"menu-operacao-aquisicao-de-dominio",
        CONTROLE_DE_ACESSO:"menu-operacao-controle-de-acesso"
        //SSE:"rnf-operacao-sse"                                ---> não existe, é comunicação interna 
        //CONTROLE_DE_EVENTO:"menu-operacao-controle-de-evento" ---> não existe, é comunicação interna 
    },
    CUSTOM_QUERY:{
        USUARIO_COM_CONTATO:"custom-query-usuario-com-contato",
        CONTROLE_DE_ACESSO_MODULOS:"custom-query-controle-de-acesso-modulos",
        TABELA_DIVERSOS_POR_NOME:"tabela-diversos-por-nome",
    }
    
}

export const MODULOS_RNF = {
    TABELA: { 
        PAIS:"rnf-tabela-pais",
        ESTADO:"rnf-tabela-estado",
        MUNICIPIO:"rnf-tabela-municipio",
        BAIRRO:"rnf-tabela-bairro",
        CEP:"rnf-tabela-cep",
        ACESSO:"rnf-tabela-acesso",        
        MODULO_DE_ACESSO:"rnf-tabela-modulo-de-acesso",        
        CNAE: "rnf-tabela-cnae",
        CNAE_SECUNDARIO: "rnf-tabela-cnae-secundario",
        NATUREZA_JURIDICA: "rnf-tabela-natureza-juridica",
        MOTIVO_SITUACAO_CADASTRAL_CNPJ: "rnf-tabela-motivo-situacao-cadastral-cnpj",        
        CNPJ: "rnf-tabela-cnpj",
        CNPJ_QUADRO_DE_SOCIO: "rnf-tabela-cnpj-quadro-de-socio",
        PRODUTO: "rnf-tabela-produto",
        TABELA_DIVERSOS: "rnf-tabela-tabela-diversos",
        TABELA_DIVERSOS_REGISTRO: "rnf-tabela-tabela-diversos-registro",
    },
    CADASTRO: { 
        CONTATO:"rnf-cadastro-contato",
        USUARIO:"rnf-cadastro-usuario",
        GRUPO_DE_USUARIO:"rnf-cadastro-grupo-de-usuario",
        DOMINIO_VIRTUAL:"rnf-cadastro-dominio-virtual"
    },
    OPERACAO: {         
        AQUISICAO_DE_DOMINIO:"rnf-operacao-aquisicao-de-dominio",
        CONTROLE_DE_ACESSO:"rnf-operacao-controle-de-acesso",
        SSE:"rnf-operacao-sse",
        CONTROLE_DE_EVENTO:"rnf-controle-de-evento"
    },
}
