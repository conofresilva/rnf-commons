import { Injectable } from '@nestjs/common';
import { Request } from 'express';

@Injectable()
export class RnfCommonService {

  dominioVirtualByRequest(request: Request):string {
    return `${request.headers['dominio-virtual']||''}`;
  }

  databaseHostByRequest(request: Request):string {
    return `${request.headers['database-host']||''}`;
  }

  databaseRouteByHost(databaseHost: string, request:Request):string {
    let result = `${databaseHost}${request.url}`;
    if (request.url.startsWith('/api/tabela/'))
      result = result.replace('/tabela/','/database/') 
      else if (request.url.startsWith('/api/cadastro/'))
        result = result.replace('/cadastro/','/database/')
        else if (request.url.startsWith('/api/operacao/'))
          result = result.replace('/operacao/','/database/');    
    return result;
  }


}

