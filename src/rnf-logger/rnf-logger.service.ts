import { LoggerService } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';

var options = {
    console: {
      level: 'debug',     // alert > error > warning > notice > info > debug
      handleExceptions: true,
      json: false,
      colorize: true,
    },
  };

export const RnfLoggerService: LoggerService = WinstonModule.createLogger({    
    exitOnError: false,
    format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.printf(msg => { return `${msg.timestamp} [${msg.level}] - ${msg.message}`; })
    ),
    transports: [        
        //new transports.File(options.file),
        new transports.Console(options.console)
    ], 
})
